import { Counter } from 'features/counter'
import React from 'react'
import Score from 'features/score'

export default function Header() {
  return (
    <div className="header">
      <Counter />
      <Score />
    </div>
  )
}
