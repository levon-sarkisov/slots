import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Reel from 'features/reel'
import Debug from 'features/debug'
import { selectCount, decrement } from 'features/counter/slice'
import { selectReel, reset } from 'features/reel/slice'
import { setScore } from 'features/score/slice'
import { selectDebugMode, selectDebugReelPos } from 'features/debug/slice'
import spinner from 'assets/img/spinner.svg'

function Game() {
  const dispatch = useDispatch()
  const reelNum = useSelector(selectReel)
  const money = useSelector(selectCount)
  const debugReelPos = useSelector(selectDebugReelPos)
  const debugMode = useSelector(selectDebugMode)
  const reel1 = useRef<any>()
  const reel2 = useRef<any>()
  const reel3 = useRef<any>()
  const [reelOne, setReelOne] = useState<number>(0)
  const [reelTwo, setReelTwo] = useState<number>(0)
  const [reelThree, setReelThree] = useState<number>(0)
  const [isRunning, setIsRunning] = useState(false)

  useEffect(() => {
    if (reelOne > -1 && reelTwo > -1 && reelThree > -1) {
      dispatch(setScore({ reelOne, reelTwo, reelThree }))
    }

    if (reelNum === 3) {
      dispatch(reset())
      setTimeout(() => {
        setIsRunning(false)
      }, 500)
    }
  }, [reelNum, reelOne, reelTwo, reelThree, dispatch])

  const play = () => {
    setIsRunning(true)
    if (!money) {
      return
    }
    if (reel1.current) {
      reel1.current.play()
    }
    if (reel2.current) {
      reel2.current.play()
    }
    if (reel3.current) {
      reel3.current.play()
    }
    dispatch(decrement())
  }

  const setPos = (next: string, current: number) => {
    return Math.round((debugReelPos[next] || 0) - current - 6 + 16)
  }

  const setNumOfRouns = (num: number, reel: string): number => {
    if (debugMode) {
      switch (reel) {
        case 'reel1':
          return setPos('reel1', reelOne)
        case 'reel2':
          return setPos('reel2', reelTwo)
        case 'reel3':
          return setPos('reel3', reelThree)
        default:
      }
    }
    return Math.round(Math.random() * 10 + num)
  }

  return (
    <div className="slot">
      <div className="flex">
        <Debug />
      </div>
      <div className="flex">
        <div className="slot__reels">
          <Reel
            ref={reel1}
            speed={80}
            onPlay={setReelOne}
            times={setNumOfRouns(10, 'reel1')}
          />
          <Reel
            ref={reel2}
            speed={50}
            onPlay={setReelTwo}
            times={setNumOfRouns(30, 'reel2')}
          />
          <Reel
            ref={reel3}
            speed={30}
            onPlay={setReelThree}
            times={setNumOfRouns(50, 'reel3')}
          />
        </div>
      </div>
      <span className="slot__spinner">
        {isRunning && <img src={spinner} alt="spinner" />}
      </span>
      <p>
        <span
          className={`slot__button ${!money || isRunning ? 'disabled' : ''}`}
        >
          <button onClick={play}>Play</button>
        </span>
      </p>
    </div>
  )
}

export default Game
