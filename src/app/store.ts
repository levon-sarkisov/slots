import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from 'features/counter/slice';
import reelReducer from 'features/reel/slice';
import scoreReducer from 'features/score/slice';
import debugReducer from 'features/debug/slice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    reel: reelReducer,
    score: scoreReducer,
    debug: debugReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
