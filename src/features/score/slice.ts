import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';

interface ReelsState {
  [reel: string]: number | null
}

interface ScoreState {
  reels: ReelsState
}

const initialState: ScoreState = {
  reels: {
    reel1: null,
    reel2: null,
    reel3: null,
  },
};

export const scoreSlice = createSlice({
  name: 'score',
  initialState,
  reducers: {
    setScore: (state, action: PayloadAction<ReelsState>) => {
      state.reels = action.payload
    },
  },
});

export const { setScore } = scoreSlice.actions;

export const selectScore = (state: RootState) => state.score.reels;

export default scoreSlice.reducer;



