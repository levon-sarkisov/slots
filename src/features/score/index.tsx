import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { selectScore } from './slice'

export default function Score() {
  const reels = useSelector(selectScore)
  const { reelOne, reelTwo, reelThree } = reels
  const [score, setScore] = useState<number>(-30)

  useEffect(() => {
    if (reelOne !== null && reelTwo !== null && reelThree !== null) {

        let s = 0

        if (reelOne === reelTwo && reelTwo === reelThree) {
          if (reelOne === 4) {
            s += 2030
          }
          if (reelOne === 3) {
            s += 1160
          }
          if (reelOne === 2) {
            s += 4150
          }
          if (reelOne === 1) {
            s += 170
          }
          if (reelOne === 0) {
            s += 30
          }
        }

        // Combination of any BAR symbols on any line 5
        // Any combination of CHERRY and 7 on any line 75 3 3xBAR symbols on any line 50
        // todo

        if (
            ((reelOne === 0 && reelTwo !== 0 && reelThree !== 0) ||
            (reelOne !== 0 && reelTwo === 0 && reelThree !== 0) ||
            (reelOne !== 0 && reelTwo !== 0 && reelThree === 0)) ||
  
            ((reelOne === 3 && reelTwo !== 3 && reelThree !== 3) ||
            (reelOne !== 3 && reelTwo === 3 && reelThree !== 3) ||
            (reelOne !== 3 && reelTwo !== 3 && reelThree === 3)) ||
  
            (reelOne === 4 && reelTwo !== 4 && reelThree !== 4) ||
            (reelOne !== 4 && reelTwo === 4 && reelThree !== 4) ||
            (reelOne !== 4 && reelTwo !== 4 && reelThree === 4)
          ) {
            s += 5
          }

          setTimeout(() => {
            setScore(score + s)
          }, 1000)
    }
  }, [reelOne, reelTwo, reelThree, setScore])

  return <div className="score">Score {score}</div>
}
