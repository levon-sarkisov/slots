import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from 'app/store'

interface DebugStateFields {
  [reel: string]: number | null
}

interface DebugState {
  debugMode: boolean
  reelsPos: DebugStateFields
}

const initialState: DebugState = {
  debugMode: false,
  reelsPos: {
    reel1: null,
    reel2: null,
    reel3: null,
  },
}

export const debugSlice = createSlice({
  name: 'debug',
  initialState,
  reducers: {
    setReelPos: (state, action: PayloadAction<DebugStateFields>) => {
      state.reelsPos = action.payload
    },
    setDebugMode: (state, action: PayloadAction<boolean>) => {
      state.debugMode = action.payload
    },
  },
})

export const { setReelPos, setDebugMode } = debugSlice.actions

export const selectDebugReelPos = (state: RootState) => state.debug.reelsPos
export const selectDebugMode = (state: RootState) => state.debug.debugMode

export default debugSlice.reducer
