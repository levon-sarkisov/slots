import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { setDebugMode, setReelPos } from './slice'

interface Props {
  onChange: Function
}

function DebugOptions({ onChange }: Props) {
  const onSelect = (e: { target: HTMLSelectElement }) => {
    onChange(parseInt(e.target.value))
  }

  return (
    <select onChange={onSelect}>
      <option value={0}>one</option>
      <option value={1}>two</option>
      <option value={2}>three</option>
      <option value={3}>7</option>
      <option value={4}>chary</option>
    </select>
  )
}

export default function Debug() {
  const [debug, setDebug] = useState(false)
  const [optionOne, setOptionOne] = useState(0)
  const [optionTwo, setOptionTwo] = useState(0)
  const [optionThree, setOptionThree] = useState(0)
  const dispatch = useDispatch()

  useEffect(() => {
    if (optionOne > -1 || optionTwo > -1 || optionThree > -1) {
      dispatch(
        setReelPos({
          reel1: optionOne,
          reel2: optionTwo,
          reel3: optionThree,
        }),
      )
    }
  }, [optionOne, optionTwo, optionThree, dispatch])

  const onDebug = () => {
    const debugMode = !debug
    setDebug(debugMode)
    dispatch(setDebugMode(debugMode))

    if (!debugMode) {
      dispatch(
        setReelPos({
          reel1: 0,
          reel2: 0,
          reel3: 0,
        }),
      )
    }
  }

  return (
    <div className="debug">
      <button onClick={onDebug}>debug</button>
      {debug && (
        <div className="debug__options">
          <DebugOptions onChange={setOptionOne} />
          <DebugOptions onChange={setOptionTwo} />
          <DebugOptions onChange={setOptionThree} />
        </div>
      )}
    </div>
  )
}
