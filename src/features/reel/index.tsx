import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { SYMBOL_HEIGHT, SPRITE_HEIGHT } from 'const'
import { useDispatch } from 'react-redux'
import { increment } from './slice'

interface Props {
  speed: number
  times: number
  onPlay: Function
}

const Reel = forwardRef(({ speed, times, onPlay }: Props, ref) => {
  const dispatch = useDispatch()
  let [posY, setPosY] = useState(0)
  const [isPlaying, setIsPlaying] = useState(false)

  useImperativeHandle(ref, () => ({
    play() {
      let i = 0
      setIsPlaying(true)

      let p = setInterval(() => {
        setPosY((posY -= SYMBOL_HEIGHT))
        ++i

        if (i === times) {
          clearInterval(p)
          setIsPlaying(false)
          dispatch(increment())
          onPlay((Math.abs(posY) % SPRITE_HEIGHT) / SYMBOL_HEIGHT)
          
        }
      }, speed)
    },
  }))
  return (
    <div
      style={{ backgroundPosition: `0px ${posY}px` }}
      className={`${isPlaying ? 'reel blur' : 'reel'}`}
    />
  )
})

export default Reel
