import { createSlice } from '@reduxjs/toolkit';
import { RootState } from 'app/store';

interface reelState {
  reelNum: number;
}

const initialState: reelState = {
  reelNum: 0,
};

export const reelSlice = createSlice({
  name: 'reel',
  initialState,
  reducers: {
    increment: state => {
      state.reelNum += 1;
    },
    reset: state => {
      state.reelNum = 0;
    },
  },
});

export const { increment, reset } = reelSlice.actions;

export const selectReel = (state: RootState) => state.reel.reelNum;

export default reelSlice.reducer;



