import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { MAX_AMOUNT } from 'const'
import { selectCount, topUp } from './slice'
import coin from 'assets/img/coin.svg'

export function Counter() {
  const count = useSelector(selectCount)
  const dispatch = useDispatch()

  const onChange = (e: { target: HTMLInputElement }) => {
    const money = parseInt(e.target.value)
    
    if (money > 0 && money <= MAX_AMOUNT) {
      dispatch(topUp(money))
    }
  }

  return (
    <div className="counter">
      <span><img src={coin} alt="" /><input value={count} onChange={onChange} /></span>
    </div>
  )
}
