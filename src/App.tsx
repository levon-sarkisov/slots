import React from 'react'
import Header from 'components/header'
import Slot from 'components/game'
import Footer from 'components/footer'

function App() {
  return (
    <React.Fragment>
      <Header />
      <Slot />
      <Footer />
    </React.Fragment>
  )
}

export default App
